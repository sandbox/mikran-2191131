(function ($) {

/**
 * Disable tabledrag changed marker.
 */
Drupal.theme.prototype.tableDragChangedMarker = function () {
  return false;
};

/**
 * Disable tabledrag default warning.
 */
Drupal.theme.prototype.tableDragChangedWarning = function () {
  return false;
};

})(jQuery);