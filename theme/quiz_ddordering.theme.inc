<?php

/**
 * @file
 * Theme functions for the drag and drop ordering question type.
 */

/**
 * Theme items section of ddordering creation form.
 *
 * @param $form
*    Renderable form array.
 */
function theme_quiz_ddordering_form($variables) {
  $form = $variables['form'];
  $header = array(
    t('Item'),
    t('Weight'),
  );
  $rows = array();
  foreach (element_children($form) as $eid) {
    $row = array();
    $row[] = drupal_render($form[$eid]['item']);
    $row[] = drupal_render($form[$eid]['weight']);
    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }
  drupal_add_tabledrag('ddordering-form', 'order', 'self', 'element-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'ddordering-form'),
  ));
}

/**
 * Theme answering section of drag and drop ordering question.
 *
 * @param $form
*    Renderable form array.
 */
function theme_quiz_ddordering_answering_form($variables) {
  $form = $variables['form'];
  $header = array();
  $rows = array();
  foreach (element_children($form) as $eid) {
    $row = array();
    $row[] = drupal_render($form[$eid]['item']);
    $row[] = drupal_render($form[$eid]['tries[' . $eid . ']']);
    $class = array('draggable');
    if (isset($form[$eid]['#attributes']['class'])) {
      $class = array_merge($class, $form[$eid]['#attributes']['class']);
    }
    $rows[] = array(
      'data' => $row,
      'class' => $class,
    );
  }
  drupal_add_tabledrag('ddordering-form', 'order', 'self', 'element-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'ddordering-form'),
  ));
}

/**
 * Theme the answer part of the node view
 *
 * @param $items
 *  Array of items. Each item is an array containing item weight and item.
 * @param $show_correct
 *  True if the user is allowed to view the solution.
 */
function theme_quiz_ddordering_answer_node_view($variables) {
  $list_items = array();
  foreach ($variables['items'] as $item) {
    $list_items[] = $item['item'];
  }
  if (!$variables['show_correct']) {
    shuffle($list_items);
  }
  return theme('item_list', array('items' => $list_items));
}
