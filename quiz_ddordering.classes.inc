<?php

/**
 * @file
 * ddordering.classes
 *
 * This module uses the question interface to define the DD ordering question.
 */

/**
 * Extension of QuizQuestion.
 */
class DDOrderingQuestion extends QuizQuestion {

  /**
   * {@inheritdoc}
   */
  public function __construct(stdClass $node) {
    parent::__construct($node);
    if (empty($this->node->items)) {
      $this->node->items = array();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function saveNodeProperties($is_new = FALSE) {
    // Loop through each item, shuffle items before inserting them to database
    // as serial id of each item is publicly visible for quiz takers and it
    // could be possible to figure out a pattern here ...
    foreach ($this->customShuffle($this->node->items) as $key => $ddorder) {
      // ddorder_id is not set so it is a new question.
      if (empty($ddorder['ddorder_id']) || $this->node->revision || isset($this->node->node_export_drupal_version)) {
        // Insert all but empty items to database.
        if (!empty($ddorder['item'])) {
          $id = db_insert('quiz_ddordering_node')
            ->fields(array(
              'nid' => $this->node->nid,
              'vid' => $this->node->vid,
              'item' => $ddorder['item'],
              'weight' => $ddorder['weight'],
            ))
            ->execute();
          $this->node->items[$key]['ddorder_id'] = $id;
        }
        else {
          unset($this->node->items[$key]);
        }
        continue;
      }
      // Empty items should be deleted
      if (empty($ddorder['item'])) {
        // remove sub item.
        db_delete('quiz_ddordering_node')
          ->condition('ddorder_id', $ddorder['ddorder_id'])
          ->execute();
        unset($this->node->items[$key]);
        continue;
      }
      // ddorder_id is set, user may update existing items.
      db_update('quiz_ddordering_node')
        ->fields(array(
          'item' => $ddorder['item'],
          'weight' => $ddorder['weight'],
        ))
        ->condition('ddorder_id', $ddorder['ddorder_id'])
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateNode(array &$form) {
    // No validation is required
  }

  /**
   * {@inheritdoc}
   */
  public function delete($only_this_version = FALSE) {
    parent::delete($only_this_version);
    if ($only_this_version) {
      $ddorder_id = db_query('SELECT ddorder_id FROM {quiz_ddordering_node} WHERE nid = :nid AND vid = :vid', array(':nid' => $this->node->nid, ':vid' => $this->node->vid))->fetchCol();
      db_delete('quiz_ddordering_user_answers')
        ->condition('ddorder_id', is_array($ddorder_id) ? $ddorder_id : array(0), 'IN')
        ->execute();

      db_delete('quiz_ddordering_node')
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
    // Delete all versions of this question.
    else {
      $ddorder_id = db_query('SELECT ddorder_id FROM {quiz_ddordering_node} WHERE nid = :nid', array(':nid' => $this->node->nid))->fetchCol();
      if (!empty($ddorder_id)) {
        db_delete('quiz_ddordering_user_answers')
          ->condition('ddorder_id', $ddorder_id, 'IN')
          ->execute();
      }
      db_delete('quiz_ddordering_node')
        ->condition('nid', $this->node->nid)
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $props = parent::getNodeProperties();

    $query = db_query('SELECT ddorder_id, item, weight FROM {quiz_ddordering_node} WHERE nid = :nid AND vid = :vid ORDER BY weight', array(':nid' => $this->node->nid, ':vid' => $this->node->vid));
    while ($result = $query->fetch()) {
      $props['items'][] = array(
        'ddorder_id' => $result->ddorder_id,
        'item' => $result->item,
        'weight' => $result->weight,
      );
    }
    $this->nodeProperties = $props;
    return $props;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeView() {
    $content = parent::getNodeView();
    $content['answers'] = array(
      '#markup' => theme('quiz_ddordering_answer_node_view', array('items' => $this->node->items, 'show_correct' => $this->viewCanRevealCorrect())),
      '#weight' => 2,
    );

    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnsweringForm(array $form_state, $rid) {
    $form = parent::getAnsweringForm($form_state, $rid);
    $form['items']['#theme'][] = 'quiz_ddordering_answering_form';
    $form['items']['#attached']['js'][] = drupal_get_path('module', 'quiz_ddordering') . '/quiz_ddordering.js';
    if (isset($rid)) {
      // The question has already been answered. We load the answers
      $response = new DDOrderingResponse($rid, $this->node);
      $responses = $response->getResponse();
      // @TODO
    }
    $items = $this->customShuffle($this->node->items);

    $default_value = 0;
    foreach ($items as $item) {
      # $form_state['storage']
      $form['items'][$item['ddorder_id']] = array(
        'item' => array(
          '#type' => 'item',
          '#markup' => $item['item']
        ),
        'tries[' . $item['ddorder_id'] . ']' => array(
          '#type' => 'textfield',
          // @see parent::getAnsweringForm(). "all form elements that takes user
          // response must have a key named 'tries'"
          '#size' => 4,
          '#attributes' => array('class' => array('element-weight')),
          '#default_value' => $default_value
        ),
      );
      $default_value++;
    }
    return $form;
  }

  /**
   * Shuffles an array, but keeps the keys.
   *
   * @param $array
   *  Array to be shuffled
   * @return
   *  A shuffled version of the array
   */
  private function customShuffle(array $array = array()) {
    $new_array = array();
    while (count($array)) {
      $element = array_rand($array);
      $new_array[$element] = $array[$element];
      unset($array[$element]);
    }
    return $new_array;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreationForm(array &$form_state = NULL) {
    $form['items'] = array(
      '#type' => 'fieldset',
      '#title' => t('Items'),
      '#weight' => -4,
      '#tree' => TRUE,
      '#theme' => 'quiz_ddordering_form',
      '#description' => t('Write your items below. For the user the items will be shuffled. Resaving this form will add extra empty items.'),
    );
    for ($i = 1; $i <= count($this->node->items)+5; ++$i) {
      $form['items'][$i]['ddorder_id'] = array(
        '#type' => 'value',
        '#default_value' => isset($this->node->items[$i -1]['ddorder_id']) ? $this->node->items[$i -1]['ddorder_id'] : ''
      );
      $form['items'][$i]['item'] = array(
        '#type' => 'textarea',
        '#rows' => 2,
        '#default_value' => isset($this->node->items[$i -1]['item']) ? $this->node->items[$i -1]['item'] : ''
      );
      $form['items'][$i]['weight'] = array(
        '#type' => 'textfield',
        '#size' => 4,
        '#default_value' => isset($this->node->items[$i -1]['weight']) ? $this->node->items[$i -1]['weight'] : $i -1,
        '#attributes' => array('class' => array('element-weight')),
      );
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaximumScore() {
    return count($this->node->items);
  }

  /**
   * Evaluates Drag and Drop Ordering answer.
   *
   * @param $answer
   *   user answer to evaluate
   */
  public function evaluateAnswer($answer) {
    $correct_answer = array();
    foreach ($this->node->items as $item) {
      if (in_array($item['weight'], $correct_answer)) {
        throw new Exception('Duplicate item weight found.');
      }
      $correct_answer[$item['ddorder_id']] = $item['weight'];
    }

    $method = variable_get('quiz_ddordering_scoring_method', SCORING_METHOD_RELATIVE);
    $score = 0;
    if ($method == SCORING_METHOD_RELATIVE) {
      // relativeScore() only counts for correct "pairs" of items. Add one to
      // that score.
      $score = 1 + $this->relativeScore($answer, $correct_answer);
    }
    elseif ($method == SCORING_METHOD_ABSOLUTE) {
      $score = count($correct_answer) - count(array_diff_assoc($answer, $correct_answer));
    }
    return $score;
  }

  /**
   * Helper function to check if two items are in correct order.
   *
   * @param $source
   *  Source array of items to be compared against target
   * @param $target
   *   Target array of items
   * @param $pair
   *   (optional) for recursive calls only.
   *
   * @return
   *   int
   */
  public function relativeScore($source, $target, $pair = NULL) {
    $prev = NULL;
    $score = 0;
    foreach ($target as $key => $weight) {
      if (!isset($prev)) {
        $prev = $key;
        continue;
      }
      // If pair is not set create a new pair and from target array and check
      // that against the source
      if (!isset($pair)) {
        $score += $this->relativeScore(array(), $source, array($prev, $key));
      }
      // If relative order of answer and correct order lists match add one to
      // score for each of these pairs and mark both members of the pair as
      // correct.
      elseif ($prev == $pair[0] && $key == $pair[1]) {
        $correct_items[$prev] = $prev;
        $correct_items[$key] = $key;
        $score++;
      }
      $prev = $key;
    }
    return $score;
  }
}

/**
 * Extension of QuizQuestionResponse
 */
class DDOrderingResponse extends QuizQuestionResponse {

  /**
   * User entered order for this question.
   */
  private $user_answer = array();

  /**
   * This of items that were correct in users response.
   */
  private $correct_items = array();

  /**
   * {@inheritdoc}
   */
  public function __construct($result_id, stdClass $question_node, $answer = NULL) {
    parent::__construct($result_id, $question_node, $answer);

    // Without answer DDOrderingResponse is being constructed for reporting
    // purposes. To make that possible prepare user answer as well as the
    // correct answer to easily comparable arrays.
    if (!isset($answer)) {
      $query = db_select('quiz_ddordering_user_answers', 'a');
      $query->join('quiz_ddordering_node', 'n', 'n.ddorder_id = a.ddorder_id');
      $query->fields('a', array('ddorder_id', 'answer'));
      $query->condition('a.result_id', $result_id);
      $query->condition('n.nid', $this->question->nid);
      $query->condition('n.vid', $this->question->vid);
      $this->user_answer = $query->execute()->fetchAllKeyed();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if (empty($this->answer)) {
      return;
    }
    $insert = db_insert('quiz_ddordering_user_answers')
      ->fields(array('ddorder_id', 'result_id', 'answer'));
    // Values should be in order but re-sort just in case, different UIs may
    // not do the same.
    asort($this->answer);
    $order = 0;
    foreach ($this->answer as $key => $weight) {
      $insert->values(array(
        'ddorder_id' => $key,
        'result_id' => $this->rid,
        'answer' => $order,
      ));
      $order++;
    }
    $insert->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    db_delete('quiz_ddordering_user_answers')
      ->condition('result_id', $this->rid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function score() {
    $question = new DDOrderingQuestion($this->question);
    return $question->evaluateAnswer($this->user_answer);
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->answer;
  }

  /**
   * {@inheritdoc}
   */
  public function getReportFormResponse($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    $header = array(t('User answer'), t('Correct Answer'));
    $rows = array();
    $items = array();
    // Prepare an array of sortable items.
    foreach ($this->question->items as $item) {
      $items[$item['ddorder_id']] = $item['item'];
    }
    // Assign user answer and correct answer to respective columns.
    foreach ($this->user_answer as $ddorder_id => $weight) {
      $rows[] = array(
        $items[$ddorder_id],
        $this->question->items[count($rows)]['item']
      );
    }

    return array(
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
  }
}
