
Overview
--------

Quiz Drag and Drop Ordering is a question type module for Quiz. Question type
is rather similiar to Matching question, and Matching module could be used to
build same questions. However Drag and Drop Ordering provides Drag and Drop
interface that suits ordering questions better than select element.
